# KnowledgeArc.Network Server

A server which multiple users can archive content to.

## Install

An installation of GraphicsMagick is required for thumbnail generation.

On Ubuntu and other Debian-based systems, use:

```
sudo apt install graphicsmagick
``` 

## Developing with Docker

Currently, this project is not set up with a container on DockerHub. Instead,
you can build the image and then deploy it using the npm script serve:docker.

Once you have cloned is Gitlab repository, cd into it and run the following to
build the Docker image:

```
docker image build -t server:latest .
```

To run the above Docker image, you can either run it using Docker or use the
npm task 'serve:docker':

```
docker container run --publish 8081:8081 --detach --name server server:latest
```

or

```
npm serve:docker
```

To tear down the running container, use:

```
docker container rm --force server
```
