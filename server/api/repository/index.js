const express = require('express')
const router = express.Router()
const FileType = require('file-type')
const { Readable } = require('stream')
const gm = require('gm')
const request = require('request')
const Stream = require('stream')

var config = require('../../config/config')

// TODO: We can't just list a bunch of pins as all kinds of data will be stored.
// For PoC purposes only.
router.route('/')
    .get(async function (req, res) {
        var pins = []

        for await (const { cid, type } of config.ipfs.node.pin.ls()) {
            pins.push({'cid': cid.toString(), 'type': type})
        }

        res.send(pins)
    })

router.route('/:hash')
    .get(async function (req, res) {
        var item
        for await (const file of config.ipfs.node.get(req.params.hash)) {
            item = file
        }

        res.send(item)
    })

// TODO: This is a poor way of handling thumbnails and needs to be replaced.
// For PoC purposes only.
router.route('/:hash/thumbnail')
    .get(async function (req, res) {
        var imageStream = null

        for await (const file of config.ipfs.node.get(req.params.hash)) {
            if (!file.content) continue

            var stream = Readable.from(file.content)

            imageStream = new Stream.PassThrough()
            stream.pipe(imageStream)

            const content = await FileType.fromStream(stream)

            if (!content || content.mime != 'image/png') {
                imageStream = null
            }
        }

        res.set('Content-Type', 'image/png')

        if (!imageStream) {
            imageStream = request.get('https://via.placeholder.com/200')
        }

        gm(imageStream)
            .resize('200', '200', '^')
            .gravity('Center')
            .crop('200', '200')
            .stream().pipe(res)
    })

module.exports = router
