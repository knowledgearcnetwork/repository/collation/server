const express = require('express')
const router = express.Router()

const repository = require('./repository')

router.use('/repository', repository)

module.exports = router
