const express = require('express')
const routes = require('./api')
const cors = require('cors')
const app = express()

const port = 3000

app.use(express.json())
app.use(cors())
app.use('/api/v1', routes)

app.get('/', (req, res) => res.send('Welcome to KnowledgeArc Network - the new way to archive'))

app.listen(port, () => console.log(`KnowledgeArc Network shared repository listening on port ${port}!`))

const config = require('./config/config')

const IPFS = require('ipfs')
let node

async function start() {
    node = await IPFS.create({ config: { 'Addresses': {
        'Swarm': [
            '/ip4/0.0.0.0/tcp/4002',
            '/ip4/127.0.0.1/tcp/4003/ws'
        ],
        'API': '/ip4/127.0.0.1/tcp/5002',
        'Gateway': '/ip4/127.0.0.1/tcp/9090'
    } } })
    const info = await node.id()
    console.log(info)
    console.log(info.id)

    const topic = info.id

    const processMsgs = (msg) => {
        pin(msg)
    }

    await node.pubsub.subscribe(topic, processMsgs)

    config.ipfs.node = node
}

async function pin(msg) {
    const hash = msg.data.toString()
    const pinned = await config.ipfs.node.pin.add(hash, { 'options': { 'timeout': 10000 }})

    console.log(pinned, 'pinned')
}

start()
