FROM node:12-alpine

RUN apk add --no-cache git

WORKDIR /usr/src/app

EXPOSE 8081

CMD [ "npm", "run", "serve:dev" ]
